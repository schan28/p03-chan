//
//  ViewController.m
//  p03-chan
//
//  Created by J on 2/13/16.
//  Copyright © 2016 Stanley Chan. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize gameScreen;
@synthesize paddle;
@synthesize ball;
@synthesize timer;
@synthesize blocksArray;

float dx = 10;
float dy = 10;

float gsMinX = 0;
float gsMaxX = 0;
float gsMinY = 0;
float gsMaxY = 0;

double ballX = 0;
double ballY = 0;
double paddleX = 0;
double paddleY = 0;

int level = 1;
double paddleWidth = 0;
NSTimeInterval ballSpeed = .04;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    


}
- (void)viewDidAppear:(BOOL)animated{
      gsMinY = CGRectGetMinY([gameScreen frame]);
      gsMaxY = CGRectGetMaxY([gameScreen frame]);
      gsMinX = CGRectGetMinX([gameScreen frame]);
      gsMaxX = CGRectGetMaxX([gameScreen frame]);
    
    blocksArray = [NSMutableArray array];
    
    //enable the start and pause button
    _start.enabled = YES;
    _stop.enabled = YES;
    
    //we do 15 for the 10 blocks and some space for margins on each side
    double blockWidth = (gsMaxX - gsMinX) / 15.0;
    
    //we do 2 to divide it by half and then by 9 for the 5 blocks and margin space
    double blockHeight = ((gsMaxY - gsMinY)/2.0) / 9.0;
    
    double currentX = gsMinX + (blockWidth*2.5);
    double currentY = gsMinY + 55;
    //want to make space 50 blocks(10 across and 5 down)
    for(int i = 0; i < 50; i++){
        if((i%10 == 0) && (i > 0)){
            currentY += blockHeight;
            currentX = gsMinX + (blockWidth*2.5);
        }
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(currentX, currentY, blockWidth, blockHeight)];
        [self.view addSubview:label];
    
        if(i < 10)
            label.backgroundColor = [UIColor colorWithRed:(153/255.0) green: (255/255.0) blue:(204/255.0) alpha:1];
        else if(i < 20)
            label.backgroundColor = [UIColor colorWithRed:(204/255.0) green: (255/255.0) blue:(153/255.0) alpha:1];
        else if (i < 30)
            label.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (255/255.0) blue:(153/255.0) alpha:1];
        else if (i < 40)
            label.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (204/255.0) blue:(153/255.0) alpha:1];
        else
            label.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (153/255.0) blue:(153/255.0) alpha:1];

        label.layer.borderColor = [UIColor darkGrayColor].CGColor;
        label.layer.borderWidth = 0.5;
        [self.blocksArray addObject:label];
        currentX += blockWidth;
    }
    
    //paddle stuff
    //so paddle starts in center
    paddleX = (gsMaxX - gsMinX)/2.0;
    //so paddle is almost at bottom of the gamescreen
    paddleY = gsMinY + (gsMaxY - gsMinY) * .8;
    paddleWidth = blockWidth * 3;
    double paddleHeight = 10;
    paddle = [[UIView alloc] initWithFrame:CGRectMake(paddleX, paddleY, paddleWidth, paddleHeight)];
    [self.view addSubview:paddle];
    [paddle setBackgroundColor:[UIColor blackColor]];
    
    //ball stuff
    //so ball starts at about 1/4 of way from left of gamescreen
    ballX = (gsMaxX - gsMinX) / 4.5;
    //starts in between paddle and last block
    ballY = (currentY + paddleY)/2.0;
    double ballWidth = 12;
    double ballHeight = 12;
    ball = [[UIView alloc] initWithFrame:CGRectMake(ballX, ballY, ballWidth, ballHeight)];
    [self.view addSubview:ball];
    [ball setBackgroundColor:[UIColor colorWithRed:(170/255.0) green: (120/255.0) blue:(211/255.0) alpha:1]];
    ball.layer.borderWidth = 0.5f;
    ball.layer.cornerRadius = 10.0;
    
    //centering the labels
    _titleTag.textAlignment = NSTextAlignmentCenter;
    _score.textAlignment = NSTextAlignmentCenter;
    _livesLeft.textAlignment = NSTextAlignmentCenter;
    _points.textAlignment = NSTextAlignmentCenter;
    _numLives.textAlignment = NSTextAlignmentCenter;
    _level.textAlignment = NSTextAlignmentCenter;
    _levelNum.textAlignment = NSTextAlignmentCenter;

    //initializing values for points, numlives, and levels
    [_points setText:[NSString stringWithFormat:@"%d", 0]];
    [_numLives setText:[NSString stringWithFormat:@"%d", 3]];
    [_levelNum setText:[NSString stringWithFormat:@"%d", 1]];
    
    //adding fonts to the labels and making them as large as possible
    [self maximizeTextSizeLabels:_titleTag];
    
    //these three labels need the same size
    [self maximizeTextSizeLabels:_score];
    [self maximizeTextSizeLabels:_livesLeft];
    [self maximizeTextSizeLabels:_level];
    //this makes the numeric labels and buttons also the same size
    [self sameTextSize:_score secondLabel:_livesLeft thirdLabel:_level fourthLabel:_points fifthLabel:_numLives sixthLabel:_levelNum firstButton:_start secondButton:_stop thirdButton:_beginNewGame];
    
    //place borders on the labels and buttons
    _titleTag.layer.borderWidth = 2;
    _score.layer.borderWidth = 1.5;
    _livesLeft.layer.borderWidth = 1.5;
    _points.layer.borderWidth = 1.5;
    _numLives.layer.borderWidth = 1.5;
    _level.layer.borderWidth = 1.5;
    _levelNum.layer.borderWidth = 1.5;

    [[_start layer] setBorderWidth:1.0f];
    [[_stop layer] setBorderWidth:1.0f];
    [[_beginNewGame layer] setBorderWidth:1.0f];

    gameScreen.layer.borderWidth = 2.0f;
    gameScreen.backgroundColor = [UIColor lightGrayColor];
    
    //place background colors on the labels and buttons
    UIColor *buttonBackColor= [UIColor colorWithRed:(255/255.0) green: (239/255.0) blue:(213/255.0) alpha:1];
    _start.backgroundColor = buttonBackColor;
    _stop.backgroundColor = buttonBackColor;
    _beginNewGame.backgroundColor = buttonBackColor;

    UIColor *labelColors = [UIColor colorWithRed:(255/255.0) green: (222/255.0) blue:(173/255.0) alpha:1];
    _level.backgroundColor = labelColors;
    _score.backgroundColor = labelColors;
    _livesLeft.backgroundColor = labelColors;

    UIColor *numberColors = [UIColor colorWithRed:(255/255.0) green: (248/255.0) blue:(220/255.0) alpha:1 ];
    _points.backgroundColor = numberColors;
    _numLives.backgroundColor = numberColors;
    _levelNum.backgroundColor = numberColors;
    
    _titleTag.backgroundColor = [UIColor colorWithRed:(255/255.0) green: (245/255.0) blue:(238/255.0) alpha:1 ];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)startAnimation:(id)sender
{
    timer = [NSTimer scheduledTimerWithTimeInterval:ballSpeed target:self selector:@selector(timerEvent:) userInfo:nil repeats:YES];
    
}

-(IBAction)stopAnimation:(id)sender
{
    [timer invalidate];
}

-(IBAction)beginNewGame:(id)sender{
    //stop the animation
    [timer invalidate];
    
    _start.enabled = YES;
    _stop.enabled = YES;
    
    //make all the blocks visible again
    for(int i = 0; i < 50; i++){
        UILabel* currentBlock = blocksArray[i];
        [currentBlock setHidden:NO];
    }
    [self resetBallPaddle];
    
    //reset the score, level, lives
    [_points setText:[NSString stringWithFormat:@"%d", 0]];
    [_numLives setText:[NSString stringWithFormat:@"%d", 3]];
    [_levelNum setText:[NSString stringWithFormat:@"%d", 1]];
}
          
-(void) maximizeTextSizeLabels: (UILabel *)l{
    int fontSize = 5;
    CGSize textSize = [[l text] sizeWithAttributes:@{NSFontAttributeName:[l font]}];
    CGFloat maxWidth = textSize.width;
    CGFloat labelWidth = CGRectGetWidth(l.frame);
    while(labelWidth > maxWidth){
        fontSize++;
        [l setFont:[UIFont systemFontOfSize:fontSize]];
        textSize = [[l text] sizeWithAttributes:@{NSFontAttributeName:[l font]}];
        maxWidth = textSize.width;
    }
    fontSize-=5;
    [l setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:fontSize]];
}

-(void) sameTextSize: (UILabel*) l1 secondLabel:(UILabel *) l2 thirdLabel:(UILabel*) l3 fourthLabel:(UILabel*) l4 fifthLabel:(UILabel*) l5 sixthLabel:(UILabel*) l6 firstButton:(UIButton*)b1 secondButton:(UIButton*)b2 thirdButton:(UIButton*)b3{
    int smallestFontSize = 100;
    int font1 = l1.font.pointSize;
    int font2 = l2.font.pointSize;
    int font3 = l3.font.pointSize;
    if(font1 < smallestFontSize){
        smallestFontSize = font1;
    }
    if(font2 < smallestFontSize){
        smallestFontSize = font2;
    }
    if(font3 < smallestFontSize){
        smallestFontSize = font3;
    }
    [l1 setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:smallestFontSize]];
    [l2 setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:smallestFontSize]];
    [l3 setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:smallestFontSize]];
    [l4 setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:smallestFontSize]];
    [l5 setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:smallestFontSize]];
    [l6 setFont:[UIFont fontWithName:@"ChalkboardSE-Bold" size:smallestFontSize]];
    [b1.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:smallestFontSize]];
    [b2.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:smallestFontSize]];
    [b3.titleLabel setFont:[UIFont fontWithName:@"AmericanTypewriter-Bold" size:smallestFontSize]];
    
}

-(void) lifeLost{
    [timer invalidate];
    int remainingLives = [[_numLives text] intValue];
    remainingLives--;
    [_numLives setText:[NSString stringWithFormat:@"%d",remainingLives]];
    if(remainingLives == 0){
        [self gameLostAlert];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oh no!" message:@"It appears you have missed the paddle and lost a life"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [self resetBallPaddle];
    }

}

-(void) resetBallPaddle{
    //reset the location of the paddle and ball
    CGPoint paddleStart;
    paddleStart.x = paddleX;
    paddleStart.y = paddleY;
    [paddle setCenter:paddleStart];
    
    CGPoint ballStart;
    ballStart.x = ballX;
    ballStart.y = ballY;
    [ball setCenter:ballStart];
    
    //reset the movement of the ball
    dx = 10;
    dy = 10;
}

- (void) determinePoints : (unsigned long)blockLocation{
    int currentScoreValue = [[_points text] intValue];
    int newScore = currentScoreValue;
    
    //blocks at the top are worth more than blocks at the bottom
    if(blockLocation < 10)
        newScore +=50;
    else if(blockLocation < 20)
        newScore +=40;
    else if(blockLocation < 30)
        newScore +=30;
    else if(blockLocation < 40)
        newScore +=20;
    else
        newScore +=10;

    [_points setText:[NSString stringWithFormat:@"%d", newScore]];

}

- (void) nextLevel {
    for(int i = 0; i < 50; i++){
        UILabel* currentBlock = blocksArray[i];
        [currentBlock setHidden:NO];
    }
    [self resetBallPaddle];
    level++;
    paddleWidth *= .9;
    CGRect frm = paddle.frame;
    frm.size.width = paddleWidth;
    paddle.frame = frm;
    [_levelNum setText:[NSString stringWithFormat:@"%d", level]];

}

- (void) gameWonAlert{
    
    //let alertController = UIAlertController(title: "Default Style", message: "A standard alert.", preferredStyle: .Alert)

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congraluations on clearing all the blocks" message:@"You may continue on to the next level.  The paddle will now shrink.  Make sure to position the paddle before you hit start."delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    //reduces paddle size
    [self nextLevel];
 
}

- (void) gameLostAlert{
    _start.enabled = NO;
    _stop.enabled = NO;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Unforunately you have ran out of lives" message:@"Hit new game to start a new game"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)timerEvent:(id)sender
{
    CGPoint p = [ball center];
    
    
    if ((p.x + dx) < gsMinX)
        dx = -dx;
    
    //top boundary
    if ((p.y + dy) < gsMinY)
        dy = -dy;
    
    //right boundary
    if ((p.x + dx) > (gsMaxX))
        dx = -dx;
    
    //bottom boundary
    bool lifeLost = false;
    if ((p.y + dy) > (gsMaxY)){
        [self lifeLost];
        lifeLost = true;
    }
    if(!lifeLost){
        p.x += dx;
        p.y += dy;
        [ball setCenter:p];
    
        // Now check to see if we intersect with paddle.  If the movement
        // has placed the ball inside the paddle, we reverse that motion
        // in the Y direction.
        if (CGRectIntersectsRect([ball frame], [paddle frame]))
        {
            
            dy = -dy;
            p.y += 2*dy;
            
            //check if the ball hit on the left or right side of paddle
            float ballCenter = CGRectGetMidX([ball frame]);
            float paddleCenter = CGRectGetMidX([paddle frame]);
            
            //if hit on left side then we move left
            if(ballCenter < paddleCenter){
                if(dx > 0){
                    dx = -dx;
                    p.x += 2*dx;
                }
            }
            //else we move to right
            if(ballCenter > paddleCenter){
                if(dx < 0){
                    dx = -dx;
                    p.x += 2*dx;
                }
            }
            [ball setCenter:p];
        }
        
        for(unsigned long i = 0; i < [blocksArray count]; i++){
            UILabel* currentBlock = blocksArray[i];
            if(CGRectIntersectsRect([ball frame], [currentBlock frame]) && (![currentBlock isHidden])){
                
                [self determinePoints:i];
                
                float ballTopY = CGRectGetMinY([ball frame]);
                float ballBottomY = CGRectGetMaxY([ball frame]);
                float currentTopY = CGRectGetMinY([currentBlock frame]);
                float currentBottomY = CGRectGetMaxY([currentBlock frame]);
                
                //if we hit from bottom or top side we want the y to negate and x to stay constant
               // if((ballTopY < currentTopY)|| (ballBottomY > currentBottomY)){
                if((ballBottomY > currentBottomY) && (dy < 0)){
                    dy = -dy;
                    p.y += 2*dy;
                }
                else if((ballTopY < currentTopY) && (dy > 0)){
                    dy = -dy;
                    p.y += 2*dy;
                }
                //if we hit from left or right side we want the x to negate and y to stay constant
                else{
                    dx = -dx;
                    p.x += 2*dx;
                }
                [ball setCenter:p];
                [currentBlock setHidden:YES];
                
                //check if we have won
                bool blocksLeft = false;
                for(unsigned long j = 0; j < [blocksArray count]; j++){
                    UILabel* currentBlockJ = blocksArray[j];

                    if(![currentBlockJ isHidden]){
                        blocksLeft = true;
                    }
                }
                if(!blocksLeft){
                    [self gameWonAlert];
                    [timer invalidate];
                }
                
                i = [blocksArray count];
            }
        }
    }
    
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    for (UITouch *t in touches)
    {
        CGPoint p = [t locationInView:self.view];
        //restricts the movement in y -axis
        //restrict movement on right boundary
        float rightBoundary = gsMaxX;
        if(((paddle.frame.size.width/2.0) + p.x) > rightBoundary){
            p.x = rightBoundary - (paddle.frame.size.width/2.0);
        }
        //restrict movement on left boundary
        float leftBoundary = gsMinX;
        if((p.x - (paddle.frame.size.width/2.0)) < leftBoundary){
            p.x = leftBoundary + (paddle.frame.size.width/2.0);
        }
        //restrict the movement in y-axis
        p.y = gsMinY + (gsMaxY - gsMinY) * .8;
        
        [paddle setCenter:p];
    }
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self touchesBegan:touches withEvent:event];
}


@end
