//
//  AppDelegate.h
//  p03-chan
//
//  Created by J on 2/13/16.
//  Copyright © 2016 Stanley Chan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

