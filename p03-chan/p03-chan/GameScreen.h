//
//  GameScreen.h
//  p03-chan
//
//  Created by J on 2/13/16.
//  Copyright © 2016 Stanley Chan. All rights reserved.
//


#import <UIKit/UIKit.h>

@interface GameScreen : UIView
{
    float dx, dy;  // Ball motion
}
@property (nonatomic, strong) UIView *paddle;
@property (nonatomic, strong) UIView *ball;
@property (nonatomic, strong) NSTimer *timer;

- (void) createPlayField;

@end
